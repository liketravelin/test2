import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


def pytest_addoption(parser):
    """Параметры для тестов"""
    parser.addoption('--env',
                     action='store',
                     default='local',
                     help='Передайте env с помощью параметра --env')


@pytest.fixture
def browser(request):
    env = request.config.getoption("--env")

    if env == 'local':
        driver = webdriver.Chrome(ChromeDriverManager().install())
    else:
        capabilities = {
            "browserName": "chrome",
            "version": "89.0",
            "platform": "LINUX"
        }
        driver = webdriver.Remote(
            command_executor="http://selenium__standalone-chrome:4444/wd/hub",
            desired_capabilities=capabilities
        )

    def fin():
        driver.quit()

    request.addfinalizer(fin)
    return driver
